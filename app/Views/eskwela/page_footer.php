<footer id="colorlib-footer">
    <div class="container">
        <div class="flex-row row-pb-md">
            <div class="justify-content-center">
                <div class="col-md-3 colorlib-widget">
                    <h4>Sponsorzy:</h4>
                             <p>
                       <?=$text_sponsorzy?>
                    </p>
                </div>
                <div class="col-md-3 colorlib-widget">
                    <h4>Szybki dostęp</h4>
                    <p>
                        <ul class="colorlib-footer-links">
                           <? foreach ($page_config->page_footer_menu as $item): ?>                          
                            <li><a href="<?=$item["url"]?>"><i class="icon-check"></i> <?=$item["name"]?></a></li>
                            <?endforeach;?>
                        </ul>
                    </p>
                </div>
                <div class="col-md-3 colorlib-widget">
                    <h4>Przydatne linki</h4>
                    <p>
                        <ul class="colorlib-footer-links">
                         
                            <? foreach ($page_config->page_footer_menu2 as $item): ?>
                                <li><a title="<?=$item["title"]?>" href="<?=$item["url"]?>" target="_blank"><i class="icon-check"></i> <?=$item["name"]?></a></li>           
                                <?endforeach;?>
                            </ul>
                        </p>
                    </div>

                    <div class="col-md-3 colorlib-widget">
                        <h4>Informacje kontaktowe</h4>
                        <ul class="colorlib-footer-links">
                            <li>ul. Stanisława Staszica 5 <br> 37-450 Stalowa Wola</li>
                            <li><a href="tel://158426945"><i class="icon-phone"></i> 15 842-69-45</a></li>
                            <li><a href="mailto:liceum@loken.pl"><i class="icon-envelope"></i> liceum@loken.pl</a></li>
                            <li><a href="http://loken.com"><i class="icon-location4"></i> loken.pl</a></li>
                        </ul>
                    </div>
                </div>
            </div>
       <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2534.4728084157746!2d22.06451115138803!3d50.562573287059756!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x473d2cc06557141b%3A0x64b2e7d854c49642!2sLiceum%20Og%C3%B3lnokszta%C5%82c%C4%85ce%20im.%20Komisji%20Edukacji%20Narodowej!5e0!3m2!1spl!2spl!4v1585736279881!5m2!1spl!2spl"
                width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""
                aria-hidden="false" tabindex="0"></iframe>-->
            </div>
            <div class="copy">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <p>
                                <small class="block">&copy; LOKEN
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                    <script>document.write(new Date().getFullYear());</script>
                                    Wszelkie prawa zatrzeżone | Szablon <a href="https://colorlib.com"
                                    target="_blank">Colorlib</a>
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                </small><br>

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>