<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=$title?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="I Liceum Ogólnokształcące im. Komisji Edukacji Narodowej w Stalowej Woli" />
    <meta name="keywords" content="liceum ogólnokształcące, ken, stalowa wola, liceum, loken, staszic" />
    <meta name="author" content="Szymon Haczyk - szymon.haczyk(at)icloud.com"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700" rel="stylesheet">


    <meta property="og:url"                content="<?=$fb["url"]?>" />
    <meta property="og:type"               content="article" />
    <meta property="og:title"              content="<?=$fb["title"]?>" />
    <meta property="og:description"        content="<?=$fb["text"]?>" />
    <meta property="og:image"              content="<?=$fb["image"]?>" />
    <!-- Animate.css -->
    <link rel="stylesheet" href="<?= base_url() ?>/themes/eskwela/css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="<?= base_url() ?>/themes/eskwela/css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="<?= base_url() ?>/themes/eskwela/css/bootstrap.css">

    <!-- Magnific Popup -->
    <link rel="stylesheet" href="<?= base_url() ?>/themes/eskwela/css/magnific-popup.css">

    <!-- Flexslider  -->
    <link rel="stylesheet" href="<?= base_url() ?>/themes/eskwela/css/flexslider.css">

    <!-- Owl Carousel -->
    <link rel="stylesheet" href="<?= base_url() ?>/themes/eskwela/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>/themes/eskwela/css/owl.theme.default.min.css">

    <!-- Flaticons  -->
    <link rel="stylesheet" href="<?= base_url() ?>/themes/eskwela/fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="<?= base_url() ?>/themes/eskwela/css/paginacja.css">
    <!-- Theme style  -->
    <link rel="stylesheet" href="<?= base_url() ?>/themes/eskwela/css/style.css">

    <!-- Modernizr JS -->
    <script src="<?= base_url() ?>/themes/eskwela/js/modernizr-2.6.2.min.js"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="js/respond.min.js"></script>
<![endif]-->
</head>
<body>

    <div class="colorlib-loader"></div>

    <div id="page">
        <nav class="colorlib-nav" role="navigation">
            <div class="upper-menu">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-4">
                            <img style="margin-right: 10px;" height="28" width="24" src="<?= base_url() ?>/upload_media/images/godlo.png"> 
                            <p>Stalowa Wola</p>
                        </div>
                        <div class="col-xs-6 col-md-push-2 text-right">
                            <p>

                                <ul class=" colorlib-social-icons" >
                                    <li><a title="Biuletyn Informacji Publicznej" href="https://bip.stalowowolski.pl">BIP</a></li>
                                    <li><a title="Okręgowa Komisja Egzaminacyjna" href="https://oke.krakow.pl">OKE</a></li>
                                    <li><a title="Kuratorium Oświaty w Rzeszowie" href="https://ko.rzeszow.pl">KO</a></li>
                                    <li><a title="Centralna Komisja Egzaminacyjna" href="https://cke.gov.pl">CKE</a></li>
                                    <li><a title="Podkarpackie Centum Edukacji Nauczycieli" href="https://www.pcen.pl/">PCEN</a></li>
                                    <li><a  href="https://www.facebook.com/LokenStalowaWola" target="_blank"><i class="icon-facebook"></i></a></li>
                                    <li><a href="/panel" target="_blank"><i class="icon-dribbble"></i></a></li>
                                </ul>
                            </p>

                        </div>
                    </div>
                </div>
            </div>
            <div class="top-menu">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4">
                            <div id="colorlib-logo"><a href="<?= base_url() ?>"><img style=" max-width: 70px; max-height: 70px;"
                             src="<?= base_url() ?>/media/logo.png" alt="" >
                             <p style="">I Liceum Ogólnokształcące <br>im. Komisji Edukacji Narodowej</p>
                         </a></div>
                     </div>
                     <div class="col-lg-8 col-md-12 text-right menu-1">
                        <ul>
                            <? foreach ($menu as $item): ?>
                                <? if ($item->hasChildren == 1): ?>
                                    <li class="<?= ($item->selected == 1 ? "active" : "") ?> <?= ($item->id == $active_menu ? "active" : "") ?> has-dropdown"><a href="<?= $item->url=="#" ? "#" :(substr($item->url, 0, 4) != "http" ? base_url($item->url) : $item->url) ?>"><?= $item->name ?></a>
                                        <ul class="dropdown">
                                            <? foreach ($item->children as $it): ?>
                                                <li class="<?= ($it->id == $active_menu ? "active" : "") ?>"><a
                                                    href="<?= (substr($it->url, 0, 4) != "http" ? base_url($it->url) : $it->url) ?>"><?= $it->name ?></a>
                                                </li>
                                            <? endforeach; ?>
                                        </ul>
                                    </li>
                                    <? elseif ($item->parent_id == 0): ?>
                                        <li class="<?= ($item->id == $active_menu ? "active" : "") ?>">
                                            <a href="<?= (substr($item->url, 0, 4) != "http" ? base_url($item->url) : $item->url) ?>"><?= $item->name ?></a>
                                        </li>
                                    <? endif; ?>
                                <? endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <?= $page_slider ?>

        <?= $page_body ?>

        <?= $page_footer ?>
    </div>


    <div class="gototop js-top">
        <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
    </div>
<?if($show_news_down):?>
    <div class="gotonews js-top_news active">
        <a href="#" class="js-gotop_news"><i class="icon-arrow-down"></i> Aktualności </a>
    </div>
<?endif;?>
    <!-- jQuery -->
    <script src="<?= base_url() ?>/themes/eskwela/js/jquery.min.js"></script>
    <!-- jQuery Easing -->
    <script src="<?= base_url() ?>/themes/eskwela/js/jquery.easing.1.3.js"></script>
    <!-- Bootstrap -->
    <script src="<?= base_url() ?>/themes/eskwela/js/bootstrap.min.js"></script>
    <!-- Waypoints -->
    <script src="<?= base_url() ?>/themes/eskwela/js/jquery.waypoints.min.js"></script>
    <!-- Stellar Parallax -->
    <script src="<?= base_url() ?>/themes/eskwela/js/jquery.stellar.min.js"></script>
    <!-- Flexslider -->
    <script src="<?= base_url() ?>/themes/eskwela/js/jquery.flexslider-min.js"></script>
    <!-- Owl carousel -->
    <script src="<?= base_url() ?>/themes/eskwela/js/owl.carousel.min.js"></script>
    <!-- Magnific Popup -->
    <script src="<?= base_url() ?>/themes/eskwela/js/jquery.magnific-popup.min.js"></script>
    <script src="<?= base_url() ?>/themes/eskwela/js/magnific-popup-options.js"></script>
    <!-- Counters -->
    <script src="<?= base_url() ?>/themes/eskwela/js/jquery.countTo.js"></script>
    <!-- Main -->
    <script src="<?= base_url() ?>/themes/eskwela/js/main.js"></script>

</body>
</html>

